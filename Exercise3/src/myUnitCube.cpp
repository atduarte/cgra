#include "myUnitCube.h"

void myUnitCube::draw()
{
	glPushMatrix();
	glNormal3f(0,0,1);


	glTranslated(-0.5,-0.5,0);

	//LADO ESQUERDO

	glPushMatrix();

	glTranslated(0,0,0.5);
	glRectd(0,0,1,1);

	glPopMatrix();

	

	//BOTTON
	glPushMatrix();

	glTranslated(0,0,0.5);
	glTranslated(0,0,-1);
	glRotated(-90,1,0,0);
	glRotated(180,1,0,0);
	
	glRectd(0,0,1,1);

	glPopMatrix();

	

	//LADO DIREITO

	glPushMatrix();

	glTranslated(0,1,-0.5);
	glRotated(180,1,0,0);

	glRectd(0,0,1,1);


	glPopMatrix();
	
	//LADO CIMA

	glPushMatrix();

	
	glTranslated(0,1,0);
	glTranslated(0,0,-0.5);
	glTranslated(0,0,1);
	glRotated(-90,1,0,0);
	
	glRectd(0,0,1,1);

	glPopMatrix();

	//LADO FRENTE

	glPushMatrix();

	
	glTranslated(1,0,0);
	glTranslated(0,0,+0.5);
	glRotated(90,0,1,0);
	//glRotated(-90,1,0,0);
	
	glRectd(0,0,1,1);

	glPopMatrix();


	//LADO Tras

	glPushMatrix();

	
	//glTranslated(1,0,0);
	glTranslated(0,0,-0.5);
	glRotated(-90,0,1,0);
	
	
	glRectd(0,0,1,1);

	glPopMatrix();




	glPopMatrix();
}