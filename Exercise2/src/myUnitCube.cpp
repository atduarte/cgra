#include "myUnitCube.h"

void myUnitCube::draw() 
{
	glTranslated(-0.5,-0.5,0.5); //Centrar na origem
	glRectd(0,0,1,1);

	glPushMatrix();
	glRotated(90,0,1,0);
	glTranslated(0,0,1);
	glNormal3d(0,0,1);
	glRectd(0,0,1,1);
	glPopMatrix();

	glPushMatrix();
	glRotated(180,0,1,0);
	glTranslated(-1,0,1);
	glNormal3d(0,0,1);
	glRectd(0,0,1,1);
	glPopMatrix();

	glPushMatrix();
	glRotated(-90,0,1,0);
	glNormal3d(0,0,-1);
	glRectd(-1,0,0,1);
	glPopMatrix();

	//Top
	glPushMatrix();
	glRotated(-90,1,0,0);
	glTranslated(0,0,1);
	glNormal3d(0,0,1);
	glRectd(0,0,1,1);
	glPopMatrix();

	//Bottom
	glPushMatrix();
	glRotated(90,1,0,0);
	glTranslated(0,-1,0);
	glNormal3d(0,0,1);
	glRectd(0,0,1,1);
	glPopMatrix();
	

}