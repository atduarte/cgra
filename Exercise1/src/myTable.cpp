#include "myTable.h"

void myTable::draw() 
{
	myUnitCube cube;

	// Pernas

	glPushMatrix();
	glScalef(0.3,3.5,0.3);

	glPushMatrix();
	glTranslated(-7,0.5,-3);
	cube.draw();
	glPopMatrix();

	glPushMatrix();
	glTranslated(7,0.5,-3);
	cube.draw();
	glPopMatrix();

	glPushMatrix();
	glTranslated(-7,0.5,3);
	cube.draw();
	glPopMatrix();

	glPushMatrix();
	glTranslated(7,0.5,3);
	cube.draw();
	glPopMatrix();

	glPopMatrix();

	// Tampo
	glPushMatrix();
	glTranslated(0,3.7,0);
	glScalef(5,0.3,3);
	cube.draw();
	glPopMatrix();

}
	
